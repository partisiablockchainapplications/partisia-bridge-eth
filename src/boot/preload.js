import { boot } from 'quasar/wrappers'
import axios from 'axios'
import Web3 from 'web3'
import { Big } from 'big.js'
import PartisiaEth from 'src/boot/eth_abi/abi_eth.json'
import PartisiaMatic from 'src/boot/eth_abi/abi_matic.json'


// Be careful when using SSR for cross-request state pollution
// due to creating a Singleton instance here;
// If any client changes this (global) instance, it might be a
// good idea to move this instance creation inside of the
// "export default () => {}" function below (which runs individually
// for each client)
const api = axios.create({ baseURL: process.env.BASE_API_URL })

export default boot(async ({ app, store, router }) => {
  const topicDeposit = Web3.utils.sha3("Deposit(uint64,bytes21,uint256)")
  const topicWithdraw = Web3.utils.sha3("Withdrawal(uint64,address,uint256,uint64,uint32)")
  // 0x01bc4bb7c8ad3891d2159f29189c74d9528104fb81b05893732d1f06621e2c4f
  console.log('topicDeposit', topicDeposit)
  console.log('topicWithdraw', topicWithdraw)

  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API

})

export const computeWithdrawMinusFee = (wei) => {
  // RoundUp always
  Big.RM = Big.roundUp
  return new Big(wei).sub(new Big(wei).times(process.env.WITHDRAW_FEE)).toFixed(0)
}

export const deriveWithdrawAmount = (amtEth, decimals) => {
  try {
    const amt = new Big(amtEth).times(`1e${decimals}`).toFixed(0)
    const amtAdj = computeWithdrawMinusFee(amt)
    return new Big(amtAdj).times(`1e-${decimals}`).toString()
  } catch (error) {
    return ''
  }
}

export const BlockchainSelection = Object.freeze({
  ETHEREUM: 1,
  POLYGON: 2,
})

export const convertDec = (amt, opts = {}) => {
  var { conversion = 1, decimals = 0, convertFromDecimals = true, removeTrailingZeros = true } = opts
  if (convertFromDecimals) conversion = `1e${decimals}`

  // seems like this doesnt need to be adjusted for the 4 decimals places
  // https://stackoverflow.com/questions/721304/insert-commas-into-number-string
  let amtStr = new Big(amt).div(conversion).toFixed(Number(decimals))
  if (amtStr.indexOf('.') !== -1) {
    while (amtStr.slice(-1) === '0' && removeTrailingZeros) {
      amtStr = amtStr.slice(0, -1)
    }
    if (amtStr.slice(-1) === '.') {
      amtStr = amtStr.slice(0, -1)
    }
  }
  if (amtStr.indexOf('.') !== -1) {
    return amtStr.replace(/\d{1,3}(?=(\d{3})+(?=\.))/g, "$&,")
  } else {
    return amtStr.replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,")
  }
}

export const getBlockchainInfo = (blockchainId) => {
  switch (blockchainId) {
    case BlockchainSelection.ETHEREUM:
      return {
        EXPLORER_TRX: process.env.ETH_EXPLORER_TRX,
        MPC_CONTRACT_DEPOSIT: process.env.MPC_CONTRACT_DEPOSIT_ETH,
        MPC_CONTRACT_WITHDRAW: process.env.MPC_CONTRACT_WITHDRAW_ETH,
        DEPOSIT_CONFIRMATIONS: Number(process.env.DEPOSIT_ETH_CONFIRMATIONS),
        ADDRESS_BYOC: process.env.ETH_ADDRESS,
        TRANSACTION_HASH: process.env.ETH_TRANSACTION,
        NETWORK_ID: Number(process.env.ETH_NETWORK_ID),
        DECIMALS: Number(process.env.ETH_DECIMALS),
        WITHDRAW_MINIMUM: process.env.ETH_WITHDRAW_MINIMUM,
        SYMBOL: 'ETH',
        ABI: PartisiaEth,
      }

    case BlockchainSelection.POLYGON:
      return {
        EXPLORER_TRX: process.env.MATIC_EXPLORER_TRX,
        MPC_CONTRACT_DEPOSIT: process.env.MPC_CONTRACT_DEPOSIT_MATIC,
        MPC_CONTRACT_WITHDRAW: process.env.MPC_CONTRACT_WITHDRAW_MATIC,
        DEPOSIT_CONFIRMATIONS: Number(process.env.DEPOSIT_MATIC_CONFIRMATIONS),
        ADDRESS_BYOC: process.env.MATIC_ADDRESS,
        TRANSACTION_HASH: process.env.MATIC_TRANSACTION,
        NETWORK_ID: Number(process.env.MATIC_NETWORK_ID),
        DECIMALS: Number(process.env.MATIC_DECIMALS),
        WITHDRAW_MINIMUM: process.env.MATIC_WITHDRAW_MINIMUM,
        SYMBOL: 'USDC',
        ABI: PartisiaMatic,
      }
  }

}

export { api }

import { store } from 'quasar/wrappers'
import { BlockchainSelection } from 'src/boot/preload'
import { createStore } from 'vuex'

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const defaultStateTab = () => {
  return {
    tab: 'deposit',
  }
}
const defaultStateContract = () => {
  return {
    metamaskAccount: null,
    blockchain: BlockchainSelection.ETHEREUM,
    partisiaContract: {},
    partisiaProps: {},
    usdcContract: {},
  }
}
const defaultStateDeposit = () => {
  return {
    depositTransactions: [],
    depositConfirmed: {},
    depositStepForm: 1,
    depositStepProgress: 1,
    withdrawTransactions: [],
  }
}

const defaultStateWithdraw = () => {
  return {
    withdrawStepForm: 2,
    withdrawAmount: '',
    withdrawEthAddress: '',
    withdrawParisiaHash: '',
    withdrawResult: [],
  }
}
export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      tab: {
        state: defaultStateTab(),
        mutations: {
          RESET_STATE_TAB: (state) => {
            Object.assign(state, defaultStateTab())
          },
          SET_TAB: (state, payload) => {
            state.tab = payload
          },
        },
        actions: {
          resetTab: ({ commit }) => {
            commit('RESET_STATE_TAB')
          },
          onTabChange: (context, payload) => {
            context.commit('SET_TAB', payload)
          },
        },
        getters: {
          tab: (state, getters) => {
            return state.tab
          },
        }
      },
      contracts: {
        state: defaultStateContract(),
        mutations: {
          RESET_STATE_CONTRACT: (state) => {
            Object.assign(state, defaultStateContract())
          },
          CONTRACT_PARTISIA: (state, payload) => {
            Object.assign(state.partisiaContract, payload)
          },
          CONTRACT_USDC: (state, payload) => {
            Object.assign(state.usdcContract, payload)
          },
          PARTISIA_PROPS: (state, payload) => {
            Object.assign(state.partisiaProps, payload)
          },
          BLOCKCHAIN_CHANGE: (state, payload) => {
            state.blockchain = payload
          },
          METAMASK_ACCOUNT: (state, payload) => {
            state.metamaskAccount = payload
          },
        },
        actions: {
          resetContracts: ({ commit }) => {
            commit('RESET_STATE_CONTRACT')
          },
          setMetamaskAccount: (context, payload) => {
            context.commit('METAMASK_ACCOUNT', payload)
          },
          onBlockchain: (context, id) => {
            context.commit('BLOCKCHAIN_CHANGE', id)
          },
          setContractPartisia: async (context, contract) => {
            context.commit('CONTRACT_PARTISIA', contract)
            await context.dispatch('refreshProps', { contract, name: 'PARTISIA' })
          },
          setContractUSDC: async (context, contract) => {
            context.commit('CONTRACT_USDC', contract)
          },
          refreshProps: async (context, payload) => {
            const contract = context.getters.contractPartisia
            const contractProps = {}
            contractProps.depositMinimum = await contract.methods.depositMinimum().call()
            contractProps.depositMaximum = await contract.methods.depositMaximum().call()
            contractProps.withdrawMaximum = await contract.methods.withdrawMaximum().call()
            contractProps.depositNonce = await contract.methods.depositNonce().call()
            contractProps.oracleNonce = await contract.methods.oracleNonce().call()

            // contractProps.epochs = await contract.methods.epochs(0).call()
            contractProps.largeOracle = await contract.methods.largeOracle().call()
            contractProps.largeOracleNonce = await contract.methods.largeOracleNonce().call()
            // contractProps.oracles = await contract.methods.oracles(largeOracleNonce).call()
            // contractProps.payments = await contract.methods.payments().call()
            // contractProps.processedWithdrawals = await contract.methods.processedWithdrawals(9).call()
            contractProps.withdrawSum = await contract.methods.withdrawSum().call()
            contractProps.getOracleSize = await contract.methods.getOracleSize().call()


            if (process.env.DEV) console.log('contractProps', JSON.stringify(contractProps, null, 2))
            // contractProps.withdrawNonce = await contract.methods.withdrawNonce().call()
            // processedWithdrawals

            context.commit(`PARTISIA_PROPS`, contractProps)
          },
        },
        getters: {
          contractPartisia: (state, getters) => {
            return state.partisiaContract
          },
          partisiaProps: (state, getters) => {
            return state.partisiaProps
          },
          contractUsdc: (state, getters) => {
            return state.usdcContract
          },
          blockchainId: (state, getters) => {
            return state.blockchain
          },
          metamaskAccount: (state, getters) => {
            return state.metamaskAccount
          },
        },
      },
      deposit: {
        state: defaultStateDeposit(),
        mutations: {
          RESET_STATE_DEPOSIT: (state) => {
            Object.assign(state, defaultStateDeposit())
          },
          SET_DEPOSIT_CONFIRMED: (state, payload) => {
            Object.assign(state.depositConfirmed, payload)
          },
          SET_DEPOSIT_STEP_FORM: (state, payload) => {
            state.depositStepForm = payload
          },
          SET_DEPOSIT_STEP_PROGRESS: (state, payload) => {
            state.depositStepProgress = payload
          },
          NEW_DEPOSIT: (state, payload) => {
            const depositTransactions = state.depositTransactions
            depositTransactions.unshift(payload)
            while (depositTransactions.length > 500) {
              depositTransactions.pop()
            }
            state.depositTransactions = [...depositTransactions]
          },
        },
        actions: {
          resetDeposit: ({ commit }) => {
            commit('RESET_STATE_DEPOSIT')
          },
          setDepositConfirmed: async (context, payload) => {
            context.commit('SET_DEPOSIT_CONFIRMED', payload)
          },
          setDepositStepForm: async (context, payload) => {
            context.commit('SET_DEPOSIT_STEP_FORM', payload)
          },
          setDepositStepProgress: async (context, payload) => {
            context.commit('SET_DEPOSIT_STEP_PROGRESS', payload)
          },
          wsDeposit: (context, payload) => {
            context.commit('NEW_DEPOSIT', payload)
          },
        },
        getters: {
          depositTransactions: (state, getters) => {
            return state.depositTransactions
          },
          withdrawTransactions: (state, getters) => {
            return state.withdrawTransactions
          },
          depositConfirmed: (state, getters) => {
            return state.depositConfirmed
          },
          depositStepForm: (state, getters) => {
            return state.depositStepForm
          },
          depositStepProgress: (state, getters) => {
            return state.depositStepProgress
          },
        },
      },
      withdraw: {
        state: defaultStateWithdraw(),
        mutations: {
          RESET_STATE_WITHDRAW: (state) => {
            Object.assign(state, defaultStateWithdraw())
          },
          WITHDRAW_AMOUNT: (state, payload) => {
            state.withdrawAmount = payload
          },
          WITHDRAW_ETH_ADDRESS: (state, payload) => {
            state.withdrawEthAddress = payload
          },
          WITHDRAW_PARTISIA_HASH: (state, payload) => {
            state.withdrawParisiaHash = payload
          },
          WITHDRAW_RESULT: (state, payload) => {
            state.withdrawResult = [...payload]
          },
          SET_WITHDRAW_STEP_FORM: (state, payload) => {
            state.withdrawStepForm = payload
          },
          // NEW_WITHDRAW: (state, payload) => {
          //   const withdrawTransactions = state.withdrawResult
          //   withdrawTransactions.unshift(payload)
          //   while (withdrawTransactions.length > 500) {
          //     withdrawTransactions.pop()
          //   }
          //   state.withdrawResult = [...withdrawTransactions]
          // },
        },
        actions: {
          resetWithdraw: (context) => {
            context.commit('RESET_STATE_WITHDRAW')
          },
          setWithdrawAmount: (context, payload) => {
            context.commit('WITHDRAW_AMOUNT', payload)
          },
          setWithdrawStep: (context, payload) => {
            context.commit('SET_WITHDRAW_STEP_FORM', payload)
          },
          setWithdrawEthAddress: (context, payload) => {
            context.commit('WITHDRAW_ETH_ADDRESS', payload)
          },
          setWithdrawParisiaHash: (context, payload) => {
            context.commit('WITHDRAW_PARTISIA_HASH', payload)
          },
          setWithdrawResult: (context, payload) => {
            context.commit('WITHDRAW_RESULT', payload)
          },
          // newWithdraw: (context, payload) => {
          //   context.commit('NEW_WITHDRAW', payload)
          // },
        },
        getters: {
          withdrawAmount: (state, getters) => {
            return state.withdrawAmount
          },
          withdrawEthAddress: (state, getters) => {
            return state.withdrawEthAddress
          },
          withdrawParisiaHash: (state, getters) => {
            return state.withdrawParisiaHash
          },
          withdrawResult: (state, getters) => {
            return state.withdrawResult
          },
          withdrawStepForm: (state, getters) => {
            return state.withdrawStepForm
          },
        },
      },
      sdkClint: {
        state: {
          sdk: null,
        },
        mutations: {
          CLEAR_SDK: (state) => {
            state.sdk = null
          },
          CONNECT_SDK: (state, payload) => {
            state.sdk = payload
          },
        },
        actions: {
          sdkClear: (context) => {
            context.commit('CLEAR_SDK')
          },
          sdkConnect: (context, payload) => {
            context.commit('CONNECT_SDK', payload)
          }
        },
        getters: {
          sdkClient: (state, getters) => {
            return state.sdk
          },
        },
      },
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})

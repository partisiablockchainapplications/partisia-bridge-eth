# PartisiaBridgeEth (partisia-bridge-eth)

Bridge ETH between Ethereum and Partisia blockchain

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
npm run dev
```


### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).



### Build the app for production
```bash
# builds two folders to dist partisia-bridge-eth and partisia-bridge-eth-testnet
npm run build
```

### serve through nginx
```conf
##########################
# bridge.mpcexplorer.com #
##########################
server {
        listen 80;
        server_name bridge.mpcexplorer.com www.bridge.mpcexplorer.com;
        return 301 https://bridge.mpcexplorer.com$request_uri;
}
server {
        listen 443;

        ssl_certificate     .../fullchain.pem;
        ssl_certificate_key .../privkey.pem;

        server_name bridge.mpcexplorer.com www.bridge.mpcexplorer.com;


        root /var/www/partisia-bridge-eth;
        index index.html;

        location /testnet {
          alias /var/www/partisia-bridge-eth-testnet;
          try_files $uri $uri/ /index.html =404;
        }

        location / {
          try_files $uri $uri/ /index.html;
        }
}
```